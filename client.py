#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""
import sys
import socket

# Cliente UDP simple.
try:
    METODO = sys.argv[1]
    USER = sys.argv[2].split("@")[0]
    IP = sys.argv[2].split("@")[1].split(":")[0]
    PORT = int(sys.argv[2].split("@")[1].split(":")[1])
    LINE = "method sip:receptor@sip_ip SIP/2.0\r\n\r\n"
except:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
# Dirección IP del servidor.


# Contenido que vamos a enviar
MESSAGE = LINE.replace("method", METODO.upper())
MESSAGE = MESSAGE.replace("receptor", USER)
MESSAGE = MESSAGE.replace("sip_ip", IP)

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PORT))

    print("Enviando: " + MESSAGE)
    my_socket.send(bytes(MESSAGE, "utf-8"))
    data = my_socket.recv(1024).decode("utf-8")

    print("Recibido -- ", data)
    if "100" in data and "180" in data and "200" in data:
        MESSAGE = LINE.replace("method", "ACK")
        MESSAGE = MESSAGE.replace("receptor", USER)
        MESSAGE = MESSAGE.replace("sip_ip", IP)
        my_socket.send(bytes(MESSAGE, "utf-8"))
        data = my_socket.recv(1024).decode("utf-8")
        print(data)
    else:
        print(data)
    print("Terminando socket...")

print("Fin.")

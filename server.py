#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import sys
import os
import socketserver


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        line = self.rfile.read().decode("utf-8")
        command = "mp32rtp -i 127.0.0.1 -p 23032 < " + AUDIO
        method = line.split()[0]
        if method == "INVITE":
            message = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\nSIP/2.0 200 OK\r\n\r\n"
            os.system(command)
        elif method == "BYE":
            message = "SIP/2.0 200 OK\r\n\r\n"
        elif method == "ACK":
            message = "SIP/2.0 200 OK\r\n\r\n"
        else:
            message = " SIP/2.0 405 Method Not Allowed\r\n\r\n"
        self.wfile.write(bytes(message, "utf-8"))

if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        AUDIO = sys.argv[3]
    except :
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
